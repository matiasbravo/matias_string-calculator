
StringCalculator = function() {

    this.add = function(values){
        if(values == '') return 0;

        if(values.indexOf('//') == 0){
           values = this.removeDelimiter(values);
        }else{
            values = this.parseDelimeters(values);
        }

        return this.sum(values); 
    };

    this.parseDelimeters = function(numsParseDelimeters){
        var delimiter = [';',',','\n'];
        numsParseDelimeters = numsParseDelimeters.toString().split(new RegExp('[' + delimiter.join('|') + ']'));
        return numsParseDelimeters;
    };

    this.removeDelimiter = function(numsRemoveDelimiter){
        numsRemoveDelimiter = numsRemoveDelimiter.replace(/\/\/+/g, '');
        numsRemoveDelimiter = this.parseDelimeters(numsRemoveDelimiter);
        numsRemoveDelimiter = numsRemoveDelimiter.filter(function(entry) { return entry.trim() != ''; });
        return numsRemoveDelimiter;
    }

    this.sum = function(numsSum){
        var  sumofnums = 0;
        if(numsSum < 0){
           throw 'negatives not allowed';
        }

        for (i = 0; i < numsSum.length; i++) {
            sumofnums += parseInt(numsSum[i]);
        }

        return sumofnums;
    };


};