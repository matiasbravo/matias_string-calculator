describe('Strings', function(){ 

    describe('add()', function(){ 
        it('should return 0 when no values present', function(){ 
             expect(new StringCalculator().add('')).toEqual(0);
        })

        it('should return an integer', function(){ 
             expect(new StringCalculator().add('1')).toEqual(1);
        })

        it('should return 3 when passing "1,2"', function(){ 
            expect(new StringCalculator().add('1,2')).toEqual(3);
        }) 

        it('should return 6 when passing "1\n2,3"', function(){ 
            expect(new StringCalculator().add('1\n2,3')).toEqual(6); 
        }) 

        it('should return 3 when passing "//;\n1;2"', function(){ 
            expect(new StringCalculator().add('//;\n1;2')).toEqual(3); 
        })

        it('should return an exception: "negatives not allowed" when passing "-1"', function(){ 
            expect(function() {new StringCalculator().sum('-1')}).toThrow(); 
        })

    });

 });